use strict;
use warnings;
use v5.20;

use Test::More;
use Path::Tiny;
use Config::Model qw/cme/;

use DhMakeRaku;
use DhMakeRaku::GitLab;
use DhMakeRaku::Config qw/dh_make_raku_config/;

subtest "setup api client" => sub {
    my $config = dh_make_raku_config;
    my $client = DhMakeRaku::GitLab::setup_api_client($config);
    ok($client,"API client is defined");
};

subtest "check existing project" => sub {
    my $config = dh_make_raku_config;
    my $client = DhMakeRaku::GitLab::setup_api_client($config);
    my $proj_data = DhMakeRaku::GitLab::get_project($config, $client, 'rakudo');
    is( $proj_data->{id},20699,"got id of rakudo project");
};


done_testing;
