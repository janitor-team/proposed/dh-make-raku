use strict;
use warnings;
use v5.20;

use Test::More;
use Path::Tiny;
use autodie qw/:all/;
use File::Copy::Recursive qw(fcopy rcopy dircopy);
use DhMakeRaku qw/setup_debian_files/;
use Test::File::Contents;

use lib qw(t/lib);
# cache file must be updated manually if the package used in tests are not in unstable.
use PkgCache;

my $email = $ENV{DEBEMAIL} = 'jb@team.org';
my $name = $ENV{DEBFULLNAME} = 'Joe Bar';
my @l = localtime;
my $year = $l[5]+1900;
my $oldyear = $year - 1;

my $test_target = shift @ARGV;

my %tests = (
    'raku-file-find' => {
        args => [ 'https://github.com/tadzik/File-Find.git', 'HEAD' ] ,
        check => [
            'control binary:raku-file-find Synopsis' => 'File::Find for Raku',
            'changelog' => qr/0\.1\.1-1/,
            'examples:raku-file-find content:0' => 'examples/*',
            'control source Build-Depends:2' => 'prove6',
            'control source Homepage' => 'https://github.com/tadzik/File-Find',
            'copyright Source' => 'https://github.com/tadzik/File-Find',
            'install:"raku-file-find.docs" content:0' => 'README.md',
            # TODO: check the content of gbp.conf (contains HEAD currently)
        ]
    },
    'raku-log' => {
        args => [ 'https://git.tyil.nl/raku/Log', 'v0.3.1' ] ,
        check => [
            'control binary:raku-log Synopsis' => 'interface for logging mechanisms to adhere to',
            'changelog' => qr/0\.3\.1-1/,
            'patches:rm-shebang Synopsis' => 'Rm shebang from Raku module files',
            'patches:rm-shebang diff' => qr(-#! /usr/bin/env false),
            'patches:rm-shebang Author:0' => $email,
        ],
        file_contents_like => [
            'debian/gbp.conf' => qr/upstream-tag = v%\(version\)s/
        ]
    },
    'raku-config' => {
        args => ['https://git.tyil.nl/raku/Config', '3.0.3'],
        check => [
            'control source Build-Depends:2' => "raku-hash-merge (>= 1.0.1)",
            'control source Build-Depends:3' => "raku-io-glob",
            'control binary:raku-config Depends:2' => "raku-hash-merge (>= 1.0.1)",
            'control binary:raku-config Depends:3' => "raku-io-glob",
            'control binary:raku-config Depends:4' => "raku-io-path-xdg (>= 0.2.0)",
            'control binary:raku-config Depends:5' => "raku-log (>= 0.3.0)"
        ],
        file_contents_like => [
            'debian/gbp.conf' => qr/upstream-tag = %\(version\)s/
        ]
    },
);

my $wr_root = path('wr_root')->absolute;

my $source_dir = path('t/samples')->absolute;

while (my ($module, $data) = each %tests) {
    next if $test_target and $module !~ qr/$test_target/;
    my $target = $wr_root->child($module);
    note("Running tests in $target");
    $target->remove_tree;
    $target->mkpath;

    dircopy(
        $source_dir->child($module)->stringify,
        $target->stringify
    ) || die "dircopy -> $target failed: $!";

    chdir($target);

    my $root = setup_debian_files (
        $target->absolute,
        $data->{args}[0],
        $module,
        $data->{args}[1],
        1,
    );

    while ($data->{check}->@*) {
        my ($path, $to_check) = splice ($data->{check}->@*, 0, 2);
        my $value = $root->grab_value($path);
        if (ref($to_check) eq 'Regexp' ) {
            like($value, $to_check,"check path '$path'");
        }
        else {
            is($value, $to_check,"check path '$path'");
        }
    }

    file_contents_like ($target->child("debian/README.source")->stringify,
                        qr/created by/, "check generation of README.source");

    while (($data->{file_contents_like} // [])->@*) {
        my ($path, $to_check) = splice ($data->{file_contents_like}->@*, 0, 2);
        file_contents_like ($target->child($path)->stringify,
                            $to_check, "check content of $path");
    }
}

done_testing;
