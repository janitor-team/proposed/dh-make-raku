use v5.20;

use Test::More;
use Path::Tiny;
use Test::File::Contents;
use Config::Model::Dpkg::Dependency;

use DhMakeRaku::RakudoStar qw/is_star_module star_module_version add_dep_in_raku_pkg/;

use lib qw(t/lib);
use PkgCache;

my $wr_root = path('wr_root')->absolute;

my $source_dir = path('t/samples')->absolute;

subtest "get rakudo info" => sub {
    my $content = path("t/modules-2022-06.txt")->slurp_utf8;
    my $data =  DhMakeRaku::RakudoStar::get_rk_star_data($content);
    like($data->{"raku-zef"}, qr/^0\.\d+\.\d+$/, "got zef version");
    is($data->{"raku-crane"}, 'master', "got Crane version");
    is($data->{"raku-config"}, '3.0.3', "got Config version");

    is($data->{"raku-foo"}, undef, "got undef for unknown package");
};

subtest "rakudo star module" => sub {
    my @tests = (
        [ __LINE__ , 'raku-log'       , '0.3.1', 1],
        [ __LINE__ , 'raku-dummy'     , undef,    0],
        [ __LINE__ , 'raku-file-find', 'master',  1],
        [ __LINE__ , 'raku-config'    , '3.0.3',  1],
    );

    while (my $unit_test = shift @tests ) {
        my ($line, $name, $version, $exp) = $unit_test->@*;
        my $res = is_star_module($name);
        is($res, $exp, "is $name star module (from line $line)");

        if ($res) {
            my $vers = star_module_version($name);
            is($vers, $version, "$name star module version (from line $line)");
        }
    }
};

subtest "tweak raku dependencies" => sub {
    my $module = 'raku';

    my $target = $wr_root->child($module);
    $target->remove_tree;
    $target->mkpath;
    my $debian = $target->child('debian');
    $debian->mkpath;
    my $control = $debian->child('control');
    $control->spew(<DATA>);

    add_dep_in_raku_pkg('raku-pi',  "3.14",$target);
    add_dep_in_raku_pkg('raku-nocheck',  "3.01",$target);

    file_contents_like($control,  qr/raku-pi \(>= 3.14\),/, "add dependency in control");
    file_contents_unlike($control,  qr/raku-pi \(>= 3.13\),/, "removed old dependency in control");

    file_contents_like($control,  qr/raku-nocheck \(>= 3.01\) <!nocheck>,/, "add dependency in control");
    file_contents_unlike($control,  qr/raku-pi \(>= 3\),/, "removed old dependency in control");

    add_dep_in_raku_pkg('raku-foo',  "master",$target);
    file_contents_like($control,  qr/raku-foo,/, "add dependency without version in control");

    add_dep_in_raku_pkg('raku-bar',  "main",$target);
    file_contents_like($control,  qr/raku-bar,/, "add dependency without version in control");

    add_dep_in_raku_pkg('raku-dont-care',  "main",'../doesnotexits');
    ok(1,"add_dep_in_raku_pkg tolerates non existent raku dir");
};

done_testing;

__DATA__
Source: raku
Maintainer: Debian Rakudo Maintainers <pkg-rakudo-devel@lists.alioth.debian.org>
Uploaders: Dominique Dumont <dod@debian.org>
Section: interpreters
Priority: optional
Build-Depends: debhelper-compat (= 13)
Standards-Version: 4.6.0
Vcs-Browser: https://salsa.debian.org/perl6-team/pkg-perl6
Vcs-Git: https://salsa.debian.org/perl6-team/pkg-perl6.git
Homepage: http://raku.org/
Rules-Requires-Root: no

Package: raku
Architecture: all
Depends: ${misc:Depends},
         perl6-readline,
         prove6,
         raku-getopt-long,
         raku-pi (>= 3.13),
         raku-nocheck (>= 3) <!nocheck>,
         raku-tap-harness,
         raku-zef,
         rakudo
Conflicts: perl6
Provides: perl6
Replaces: perl6
Description: Compiler for Raku language
 Raku (formerly known as Perl6) is a programming language, member of the
 Perl family. Like Perl 5, her world-famous big sister, Raku intends to
 carry forward the high ideals of the Perl community and is currently
 being developed by a team of dedicated and enthusiastic volunteers.
 .
 raku package is a metapackage that aims to depend on a Raku
 compiler and on the Raku core modules shipped with rakudo-star.
 These modules will be added once they are available on Debian.
 .
 Raku version number represents the version of the language specification.
 The version of the compiler is another matter.
