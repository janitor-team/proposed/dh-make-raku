#! /usr/bin/env false

use v6.d;

use Log::Level;

#| The role for a Log implementation.
unit role Log::Implementation;

# Methods for handling plain string messages.
multi method emergency (Str:D $) { * }
multi method alert (Str:D $) { * }
multi method critical (Str:D $) { * }
multi method error (Str:D $) { * }
multi method warning (Str:D $) { * }
multi method notice (Str:D $) { * }
multi method info (Str:D $) { * }
multi method debug (Str:D $) { * }

# Methods for handling formatted messages.
multi method emergency (Str:D $template, *@args) { self.emergency($template.fmt(|@args)) }
multi method alert (Str:D $template, *@args) { self.alert($template.fmt(|@args)) }
multi method critical (Str:D $template, *@args) { self.critical($template.fmt(|@args)) }
multi method error (Str:D $template, *@args) { self.error($template.fmt(|@args)) }
multi method warning (Str:D $template, *@args) { self.warning($template.fmt(|@args)) }
multi method notice (Str:D $template, *@args) { self.notice($template.fmt(|@args)) }
multi method info (Str:D $template, *@args) { self.info($template.fmt(|@args)) }
multi method debug (Str:D $template, *@args) { self.debug($template.fmt(|@args)) }

# Method for configuration
multi method add-output (IO::Handle:D $, Int() $ where Log::Level::Emergency ≤ * ≤ Log::Level::Debug, Callable $?) { * }

=begin pod

=NAME    Log::Implementation
=VERSION 0.3.1
=AUTHOR  Patrick Spek <p.spek@tyil.nl>

=begin LICENSE
Copyright © 2020

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License along
with this program.  If not, see http://www.gnu.org/licenses/.
=end LICENSE

=end pod

# vim: ft=raku noet sw=8 ts=8
