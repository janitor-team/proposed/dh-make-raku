#! /usr/bin/env false

use v6.d;

enum Log::Level <
	Emergency
	Alert
	Critical
	Error
	Warning
	Notice
	Info
	Debug
>;

=begin pod

=NAME    Log::Level
=VERSION 0.3.8
=AUTHOR  Patrick Spek <p.spek@tyil.nl>

=head1 Description

C<Log::Level> is an C<Enum> to allow for human-readable log levels in your
code, as opposed to magic numbers that give little insight to people not
familiar with the levels. The allowed values are:

=item C<Log::Level::Emergency>
=item C<Log::Level::Alert>
=item C<Log::Level::Critical>
=item C<Log::Level::Error>
=item C<Log::Level::Warning>
=item C<Log::Level::Notice>
=item C<Log::Level::Info>
=item C<Log::Level::Debug>

=begin LICENSE
Copyright © 2020

This program is free software: you can redistribute it and/or modify it under
the terms of the GNU Lesser General Public License as published by the Free
Software Foundation, version 3.

This program is distributed in the hope that it will be useful, but WITHOUT ANY
WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A
PARTICULAR PURPOSE.  See the GNU Lesser General Public License for more
details.

You should have received a copy of the GNU Lesser General Public License along
with this program.  If not, see http://www.gnu.org/licenses/.
=end LICENSE

=end pod

# vim: ft=raku noet sw=8 ts=8
