use strict;
use warnings;
use v5.20;

use Test::More;
use Path::Tiny;
use Config::Model qw/cme/;

use DhMakeRaku;
use DhMakeRaku::Config;

subtest "read git credentials" => sub {
    my $config = DhMakeRaku::Config::user_config;
    my $cred_file = path("/tmp/dummy-git-creds");
    $cred_file->spew('https://dod:notmytoken@salsa.debian.org');
    DhMakeRaku::Config::read_git_credentials($config, $cred_file);
    is($config->{user},"dod","check user retrieved from git credentials");
    is($config->{private_token},"notmytoken","check token retrieved from git credentials");
};

subtest "config from env" => sub {
    my $config = DhMakeRaku::Config::user_config;
    $ENV{DRT_SALSA_USER}="foo";
    DhMakeRaku::Config::udpate_config_from_environment ($config);
    is($config->{user},"foo","check user retrieved from environment");
};


done_testing;
